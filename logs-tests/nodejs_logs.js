const fs = require('fs');

function generateLogMessage(level, message) {
    const timestamp = new Date().toISOString();
    return `${timestamp} [${level.toUpperCase()}]: ${message}\n`;
}

function writeLogToFile(logFile, level, message) {
    const logMessage = generateLogMessage(level, message);
    fs.appendFile(logFile, logMessage, (err) => {
        if (err) {
            console.error('Error writing to log file:', err);
        }
    });
}

const logFile = 'nodejs_logs.log';

function sendLogsRegularly(logFile) {
    const interval = 5000;
    
    function sendLog() {
        writeLogToFile(logFile, 'info', 'This is JavaScript log message.');
    }
    
    sendLog();
    
    const intervalId = setInterval(sendLog, interval);
    
    setTimeout(() => {
        clearInterval(intervalId);
        console.log('Arrêt de l\'envoi des logs réguliers.');
    }, 30000);
}

sendLogsRegularly(logFile);





// Example usage:
writeLogToFile(logFile, 'info', 'This is an information log message.');
writeLogToFile(logFile, 'error', 'This is an error log message.');

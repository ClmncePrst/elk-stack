import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.*;

public class LoggingExample {

    private final static Logger LOGGER = Logger.getLogger(LoggingExample.class.getName());

    public static void main(String[] args) {
        // Configuration du logging
        Handler fileHandler;
        try {
            fileHandler = new FileHandler("java_logs.log");
            LOGGER.addHandler(fileHandler);
            SimpleFormatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Erreur lors de la configuration du fichier de logs", e);
        }

        // Exemple de logs
        LOGGER.info("Ceci est un message de log INFO");
        LOGGER.warning("Ceci est un message de log WARNING");
        LOGGER.severe("Ceci est un message de log ERROR");
    }
}

import logging
import time

logging.basicConfig(filename='python_logs.log', level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

while True:
    logging.info('This is a Python log message')
    time.sleep(2)

